1. To set up the database create a new database called 'rest' and then paste the following code into the 'SQL' query tab of your new database:
this will create the new table with structure and populate it with some data



SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE TABLE `staff` (
  `Staff_Id` int(11) NOT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `Campus` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;



INSERT INTO `staff` (`Staff_Id`, `LastName`, `FirstName`, `Campus`) VALUES
(1, 'Newman', 'Hank', 'belfast'),
(3, 'Kay', 'Mandy', 'Belfast'),
(4, 'Lancaster', 'Bert', 'Jordanstown'),
(8, 'ringo', 'hibo', 'belfast'),
(9, 'Rose', 'Pink', 'Belfast');


ALTER TABLE `staff`
  ADD PRIMARY KEY (`Staff_Id`);


ALTER TABLE `staff`
  MODIFY `Staff_Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
  
  
  
  
  
2. In the api.php file change the username and password at the bottom in the function 'getConnection()' to your own.
 
3. run everything from localhost
  
4. DONE