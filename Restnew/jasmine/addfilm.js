describe("Check addFilm function.", function(){
	it("Should make an AJAX request to add film information into the film table.",
		function(){
			spyOn($, "ajax");
			addFilm();
			expect($.ajax).toHaveBeenCalledWith({
				type: "POST",
				url: requestUrl,
				dataType: jsonType,
				success: processResponse
			});
		});
});