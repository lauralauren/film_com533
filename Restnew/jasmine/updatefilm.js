describe("Check updateFilm function.", function(){
	it("Should make an AJAX request to update film information.",
		function(){
			spyOn($, "ajax");
			updateFilm();
			expect($.ajax).toHaveBeenCalledWith({
				type: "PUT",
				url: requestUrl,
				dataType: jsonType,
				success: processResponse
			});
		});
});