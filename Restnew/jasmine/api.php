<?php
	require 'Slim/Slim.php';
	Slim\Slim::registerAutoloader();
	use Slim\Slim;
	$app = new Slim(); 
	$app->get('/films', 'getFilms'); 
	$app->get('/films/:id', 'getFilm');
	$app->post('/films', 'addFilm');
	$app->put('/films/:id', 'updateFilm');
	$app->delete('/films/:id', 'deleteFilm');
	$app->run();
	
	function deleteFilm($id) {
		$request = Slim::getInstance()->request();
		$body = $request->getBody();
		$film = json_decode($body);
		$sql = "DELETE FROM film WHERE Film_id=:id";
		try { 
			$db = getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $id); 
			$stmt->execute();
			$db = null; 
			responseJson(json_encode($film),200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function updateFilm($id) {
		$request = Slim::getInstance()->request();
		$body = $request->getBody();
		$film = json_decode($body);
		$sql = "UPDATE film SET title=:title, category=:category,
		description=:description WHERE film_id=:id"; 
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("title", $film->title);
			$stmt->bindParam("category", $film->category);
			$stmt->bindParam("description", $film->description);
			$stmt->bindParam("id", $film->id); 
			$stmt->execute();
			$db = null; 
			responseJson(json_encode($film),200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function getFilm($id) {
		$sql = "SELECT * FROM film WHERE film_id=:id"; 
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $id); 
			$stmt->execute();
			$film = $stmt->fetchObject();
			$db = null; 
			responseJson(json_encode($film),200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function addFilm(){
		$request=Slim::getInstance()->request();
		$film=json_decode($request->getBody());
		$sql = "INSERT into film (title, category, description)values(:title, :category, :description)";
		try{
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("title", $film->title);
			$stmt->bindParam("category", $film->category);
			$stmt->bindParam("description", $film->description);
			$stmt->execute();
			$film->Film_Id=$db->lastInsertId();
			$db = null;
			responseJson(json_encode($film),201);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function getFilms() {
		$sql = "SELECT * FROM film ORDER BY Film_Id";
		try{
			$db = getConnection();
			$stmt = $db->query($sql);
			$films = $stmt->fetchAll(PDO::FETCH_OBJ);
			$db = null;
			responseJson('{"film":'.json_encode($films).'}',200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function getConnection(){
		$dbhost = "localhost";		//CHANGE THIS TO YOUR HOST
		$dbuser = "B00614013";			//CHANGE THIS TO YOUR USERNAME
		$dbpassword ="J8nkdZAX";		//CHANGE THIS TO YOUR PASSWORD
		$dbname = "B00614013";			//CHANGE THIS TO YOUR DATABASE NAME
		$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpassword);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbh;
		
	}
	
	function responseJson($responseBody,$statusCode){
		$app = Slim::getInstance();
		$response = $app->response();
		$response['Content-Type']='application/json';
		$response->status($statusCode);
		$response->body($responseBody);
	}
		
?>
