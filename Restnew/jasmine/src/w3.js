$(document).ready(function(){	
$.ajax({
		type:'GET',
		dataType: "json",
		url: "api.php/films",
		success: showAllFilms,
		error: showError
	});
	
	$("#btnSave").click(function(){
		var film = new Film(
		$("#title").val(),
		$("#category").val(),
		$("#description").val()
		);
		
		$.ajax({
			type:'POST',
			dataType: "json",
			url: "api.php/films",
			data: JSON.stringify(film),
			success: showResponse,
			error: showError
		});
	});
	

	$("#btnUpdate").click(function(){
		var film = new Film2(
		$("#updatetitle").val(),
		$("#updatecategory").val(),
		$("#updatedescription").val(),
		$("#updateId").val()
		);
		
		var id = $("#updateId").val();
		
		$.ajax({
			type:'PUT',
			dataType: "json",
			url: "api.php/films/"+id,
			data: JSON.stringify(film),
			success: showResponse,
			error: showError
		});
	});
	
	$("#btnDelete").click(function(){
		
		var id = $("#deleteId").val();
		
		$.ajax({
			type:'DELETE',
			dataType: "json",
			url: "api.php/films/"+id,
			success: showResponse,
			error: showError
		});
	});
	
	
	$("#btnGet").click(function(){
		var id = $("#id").val();
		
		$.ajax({
			type:'GET',
			dataType: "json",
			url: "api.php/films/"+id,
			success: showFilm,
			error: showError
		});
	});
	
});

function showAllFilms(responseData){
	$.each(responseData.film,function(index,film){
		$("#film_list").append("<li type='square'> Film Id:"+film.Film_Id+", Title:" +film.title+", Category:"+film.category+",Description:"+ film.description);
		$("#film_list").append("</li>")
	});
}

function showError(responseData){
	alert("Sorry a problem has occurred");
	console.log(responseData);
}


function showFilm(responseData){
		$("#single_film_list").append("<li type='square'> FilmId:"+responseData.Film_Id+", title:" +responseData.title+" "+responseData.category+",description:"+ responseData.description);
		$("#single_film_list").append("</li>");
}

function showError(responseData){
	alert("Sorry a problem has occurred");
	console.log(responseData);
}

function Film(title, category, description){
	this.title=title;
	this.category=category;
	this.description=description
}

function Film2(title, category, description, id){
	this.title=title;
	this.category=category;
	this.description=description
	this.id=id
}

function showResponse(responseData){
	console.log(responseData);
}