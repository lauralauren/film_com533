describe("Spy on deleteFilm function", function() {
it("should make an AJAX request to delete film based on the ID entered",
 function() {
spyOn($,"ajax");
deleteFilm();
expect($.ajax).toHaveBeenCalledWith({
type:'DELETE',
dataType: "json",
url: requestUrl,
success: processResponse,
 }); 
}); 
});
