//GET//
$(document).ready(function(){	
$.ajax({
		type:'GET',
		dataType: "json",
		url: "api.php/films",
		success: showAllFilms,
		error: showError
	});
	
	function showAllFilms(responseData){
	$.each(responseData.film,function(index,film){
		$("#film_list").append("<li type='square'> Film Id:"+film.Film_Id+", Title:" +film.title+", Category:"+film.category+",Description:"+ film.description);
		$("#film_list").append("</li>")
	});
}
function showError(responseData){
	alert("Sorry a problem has occurred");
	console.log(responseData);
}

//ADD//
$(document).ready(function(){	
	$("#btnSave").click(function(){
		var film = new film($("#title").val(),$("#category").val(),$("#description").val());
		$.ajax({
			type:'POST',
			dataType: "json",
			url: "api.php/films",
			data: JSON.stringify(film),
			success: showResponse,
			error: showError
		});
	});
	
	function Film(title, category, description){
	this.title=title;
	this.category=category;
	this.description=description
}

//UPDATE//
$(document).ready(function(){	
	$("#btnUpdate").click(function(){
	var film= new film($("#id").val(),$("update_title").val(),$("update_category").val(),$("update_description").val(),$("#updateId").val());
	$.ajax({
			type:'PUT',
			dataType: "json",
			url: "api.php/films/"+$("#Id").val(),
			data: JSON.stringify(film),
			success: showResponse,
			error: showError
		});
	});
	});
	
	function showResponse(){
	alert ("successful");
	}
	
	function showError(){
	alert ("Sorry a problem has occurred");
	}
	
	
	//DELETE//
		
	$("#btnDelete").click(function(){
		
		var film= delete Film($("#Id").val());
		
		$.ajax({
			type:'DELETE',
			dataType: "json",
			url: "api.php/films/"+$("#Id").val(),
			success: showResponse,
			error: showError
		});
	});
	function showError(responseData){
	alert("Sorry a problem has occurred");
	console.log(responseData);
}

	function showResponse(){
	alert ("successful");
	}
	
	//GET
	
	$("#btnGet").click(function(){
		var id = $("#id").val();
		
		$.ajax({
			type:'GET',
			dataType: "json",
			url: "api.php/films/"+id,
			success: showFilm,
			error: showError
		});
	});
	
});
function showResponse(){
	alert ("successful");
	}
	

function showError(responseData){
	alert("Sorry a problem has occurred");
	console.log(responseData);
}