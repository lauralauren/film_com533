describe("Spy on addFilm function", function() {
it("should make an AJAX request to add film to database",
function() {
spyOn($,"ajax");
addFilm();
expect($.ajax).toHaveBeenCalledWith({
type:"POST",
url:requestUrl,
dataType:"json",
success:processResponse
 }); 
}); 
});