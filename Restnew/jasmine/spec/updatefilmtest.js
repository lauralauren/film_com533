describe("Spy on updateFilm function", function() {
it("should make an AJAX request to update films based on the ID entered",
 function() {
spyOn($,"ajax");
updateFilm();
expect($.ajax).toHaveBeenCalledWith({
type:'PUT',
dataType: "json",
url: requestUrl,
success: processResponse,
 }); 
}); 
});