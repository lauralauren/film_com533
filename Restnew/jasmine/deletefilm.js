describe("Check deleteFilm function.", function(){
	it("Should make an AJAX request to delete film using the Film Id.",
		function(){
			spyOn($, "ajax");
			deleteFilm();
			expect($.ajax).toHaveBeenCalledWith({
				type: "DELETE",
				url: requestUrl,
				dataType: jsonType,
				success: processResponse
			});
		});
});