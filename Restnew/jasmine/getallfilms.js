describe("Check getFilms function.", function(){
	it("Should make an AJAX request to retrieve all the films from the Database.",
		function(){
			spyOn($, "ajax");
			getFilms();
			expect($.ajax).toHaveBeenCalledWith({
				type: "GET",
				url: requestUrl,
				dataType: jsonType,
				success: processResponse
			});
		});
});