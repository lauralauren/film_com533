describe("Spy on findById function.", function(){
	it("Should make an AJAX request to get film by the user inserting information and then finding film by id.",
		function(){
			spyOn($, "ajax");
			findById();
			expect($.ajax).toHaveBeenCalledWith({
				type: "GET",
				url: requestUrl,
				dataType: jsonType,
				success: processResponse
			});
		});
});