$(document).ready(function(){
	$.ajax({
		type:'GET',
		dataType: "json",
		url: "api.php/staffs",
		success: showAllStaffs,
		error: showError
	});
	
	$("#btnSave").click(function(){
		var staff = new Staff(
		$("#firstName").val(),
		$("#lastName").val(),
		$("#campus").val()
		);
		
		$.ajax({
			type:'POST',
			dataType: "json",
			url: "api.php/staffs",
			data: JSON.stringify(staff),
			success: showResponse,
			error: showError
		});
	});
	
	$("#btnUpdate").click(function(){
		var staff = new Staff2(
		$("#updateFirstName").val(),
		$("#updateLastName").val(),
		$("#updateCampus").val(),
		$("#updateId").val()
		);
		
		var id = $("#updateId").val();
		
		$.ajax({
			type:'PUT',
			dataType: "json",
			url: "api.php/staffs/"+id,
			data: JSON.stringify(staff),
			success: showResponse,
			error: showError
		});
	});
	
	$("#btnDelete").click(function(){
		
		var id = $("#deleteId").val();
		
		$.ajax({
			type:'DELETE',
			dataType: "json",
			url: "api.php/staffs/"+id,
			success: showResponse,
			error: showError
		});
	});
	
	
	$("#btnGet").click(function(){
		var id = $("#id").val();
		
		$.ajax({
			type:'GET',
			dataType: "json",
			url: "api.php/staffs/"+id,
			success: showStaff,
			error: showError
		});
	});
	
});

function showAllStaffs(responseData){
	$.each(responseData.staff,function(index,staff){
		$("#staff_list").append("<li type='square'> Staff Id:"+staff.Staff_Id+", FullName:" +staff.FirstName+" "+staff.LastName+",Campus:"+ staff.Campus);
		$("#staff_list").append("</li>")
	});
}

function showStaff(responseData){
		$("#single_staff_list").append("<li type='square'> StaffId:"+responseData.Staff_Id+", FullName:" +responseData.FirstName+" "+responseData.LastName+",Campus:"+ responseData.Campus);
		$("#single_staff_list").append("</li>");
}

function showError(responseData){
	alert("sorry there was a problem");
	console.log(responseData);
}

function Staff(firstName, lastName, campus){
	this.firstname=firstName;
	this.lastname=lastName;
	this.campus=campus
}

function Staff2(firstName, lastName, campus, id){
	this.firstname=firstName;
	this.lastname=lastName;
	this.campus=campus
	this.id=id
}

function showResponse(responseData){
	console.log(responseData);
}
