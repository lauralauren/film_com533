<?php
	require 'Slim/Slim.php';
	Slim\Slim::registerAutoloader();
	use Slim\Slim;
	$app = new Slim(); 
	$app->get('/staffs', 'getStaffs'); 
	$app->get('/staffs/:id', 'getStaff');
	$app->post('/staffs', 'addStaff');
	$app->put('/staffs/:id', 'updateStaff');
	$app->delete('/staffs/:id', 'deleteStaff');
	$app->run();
	
	function deleteStaff($id) {
		$request = Slim::getInstance()->request();
		$body = $request->getBody();
		$staff = json_decode($body);
		$sql = "DELETE FROM staff WHERE staff_id=:id";
		try { 
			$db = getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $id); 
			$stmt->execute();
			$db = null; 
			responseJson(json_encode($staff),200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function updateStaff($id) {
		$request = Slim::getInstance()->request();
		$body = $request->getBody();
		$staff = json_decode($body);
		$sql = "UPDATE staff SET firstname=:firstname, lastname=:lastname,
		campus=:campus WHERE staff_id=:id"; 
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("firstname", $staff->firstname);
			$stmt->bindParam("lastname", $staff->lastname);
			$stmt->bindParam("campus", $staff->campus);
			$stmt->bindParam("id", $staff->id); 
			$stmt->execute();
			$db = null; 
			responseJson(json_encode($staff),200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function getStaff($id) {
		$sql = "SELECT * FROM staff WHERE staff_id=:id"; 
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $id); 
			$stmt->execute();
			$staff = $stmt->fetchObject();
			$db = null; 
			responseJson(json_encode($staff),200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function addStaff(){
		$request=Slim::getInstance()->request();
		$staff=json_decode($request->getBody());
		$sql = "INSERT into staff (firstname, lastname, campus)values(:firstname, :lastname, :campus)";
		try{
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("firstname", $staff->firstname);
			$stmt->bindParam("lastname", $staff->lastname);
			$stmt->bindParam("campus", $staff->campus);
			$stmt->execute();
			$staff->Staff_Id=$db->lastInsertId();
			$db = null;
			responseJson(json_encode($staff),201);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function getStaffs() {
		$sql = "SELECT * FROM staff ORDER BY Staff_Id";
		try{
			$db = getConnection();
			$stmt = $db->query($sql);
			$staffs = $stmt->fetchAll(PDO::FETCH_OBJ);
			$db = null;
			responseJson('{"staff":'.json_encode($staffs).'}',200);
		}catch(PDOException $e){
			responseJson('{"error":{"text":'.$e->getMessage().'}}',500);
		}
	}
	
	function getConnection(){
		$dbhost = "localhost";		//CHANGE THIS TO YOUR HOST
		$dbuser = "B00614013";			//CHANGE THIS TO YOUR USERNAME
		$dbpassword = "J8nkdZAX";		//CHANGE THIS TO YOUR PASSWORD
		$dbname = "B00614013";			//CHANGE THIS TO YOUR DATABASE NAME
		$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpassword);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbh;
		
	}
	
	function responseJson($responseBody,$statusCode){
		$app = Slim::getInstance();
		$response = $app->response();
		$response['Content-Type']='application/json';
		$response->status($statusCode);
		$response->body($responseBody);
	}
?>
